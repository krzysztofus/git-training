# git-training

Git exercises

## Getting started
This `readme` file will contain a step by step instructions for the workshop. Have a look at [useful links](#useful-links) at the end of the file for additional information

## Install git

Install git following these [instructions](https://git-scm.com/download/mac)

## Workshop contents

### Prepare a workspace directory
```shell
cd ~ #change directory to the home dir
mkdir -p trainings/git-training #make your project directory creating parent directories if needed
```
### Create git repository in [gitlab.com](gitlab.com)
1. Create an account
2. Create a new blank project by clicking [New project](https://gitlab.com/projects/new) button
![](readme-resources/create-project.png)
3. Clone your remote repository locally using one of the methods (https or ssh) provided `Clone` dropdown button
```shell
cd ~/trainings/git-training/ #enter your workspace
git clone https://gitlab.com/krzysztofus/git-training.git #downloads repository contents via https with basic auth
#or
git clone git@gitlab.com:krzysztofus/git-training.git #downloads repository contents via ssh (requires registering ssh public key in the gitlab account profile)
```
### Prepare example application to version with git
1. Use [springboot project generator](https://start.spring.io/) to generate empty project and download it
2. Move project archive and unzip it
```shell
mv ~/Downloads/git-training.zip ~/trainings/
cd ~/trainings/
unzip git-trainings.zip
```
> :bulb: You can also initialize an empty repository locally with `git init` in your current directory

### Stashing changes
```shell
#Make some changes in the repository
git status #observe the difference
git stash
git status #observe the difference
git stash list #print stash changes stack
git stash show #print top stashed changes
git stash pop #take top change out of a stash and apply it on a file system
```

### Add project files to the stage/index
```shell
cd ~/trainings/git-training/ #enter your workspace
git status #display difference
git add -A #adds all files to the index
#or
git add [<files>...] #hand-pick files to be added to the index
git status #observe the files being added to the index
```

> :bulb: If you made a mistake you can reset the index by executing `git reset HEAD`. More on that later...

### Add `.gitignore` file
The `.gitignore` file marks files and directories which should not be considered in the scope of git commands
Use any text editor to create the file and fill it in with an example content
```shell
#Filter out build related artifacts
target/
!.mvn/wrapper/maven-wrapper.jar
!**/src/main/**/target/
!**/src/test/**/target/

### STS ###
.apt_generated
.classpath
.factorypath
.project
.settings
.springBeans
.sts4-cache

### IntelliJ IDEA ###
.idea
*.iws
*.iml
*.ipr

### NetBeans ###
/nbproject/private/
/nbbuild/
/dist/
/nbdist/
/.nb-gradle/
build/
!**/src/main/**/build/
!**/src/test/**/build/

### VS Code ###
.vscode/
```

### Commit files and push the commit to the remote repository
1. Commit files
```shell
git status #have a look at differences
git commit -m "Message with a mistake"
git status #all files should be commited at this point
```
2. Amend the commit message
```shell
git commit --amend
```
> :bulb: Amend can also be used to e.g. append additional indexed changes to the latest commit
3. Push commit to the remote
```shell
git push
```

> :bulb: You can check the remote details by typing
> ```shell
> C02ZK6ADLVDL:git-training krzysztof.fus$ git remote -v
> origin	https://gitlab.com/krzysztofus/git-training.git (fetch)
> origin	https://gitlab.com/krzysztofus/git-training.git (push)
> ```

### Working with branches
1. Create a branch
```shell
git checkout -b MyBranch #create new local branch of name MyBranch
git status #observe difference
git branch #list currently existing local branches
git branch -a #list all branches (including remote(s))
```
2. Navigate back to master
```shell
git checkout main #checkout branch of name main. You can checkout any existing local branch this way
```
> :bulb: You can quickly navigate to a previous branch by typing `git checkout -`
3. Create few more branches and observe the difference
4. Remove obsolete branches
```shell
git branch -d BranchName
```
5. Make some change in the code and commit it
6. Push the committed code to the remote
```shell
git push --set-upstream origin MyBranch #pushes changes to the remote of name `origin` to branch of name `MyBranch` and sets that branch as a tracked upstream
```
7. Make some additional changes and push them to the remote branch as another commit
```shell
git push
```
> :bulb: You can check upstream details by typing
> ```shell
> C02ZK6ADLVDL:git-training krzysztof.fus$ git branch -vv
> * MyBranch e72e8b7 [origin/MyBranch] Branch developed change
>   main     10a9e19 [origin/main] Commiting and pushing
> ```

> :bulb: You can forcefully push your changes to the remote overriding its history 
> ```shell
> git push -f
> ```
> By default force push is not accepted in gitlab unless explicitly enabled in the repository settings

### Merge requests
1. In gitlab visit `Repository > Branches`
![](readme-resources/merge-request.png)
2. Create a new merge request by clicking `Merge request` button next to your branch name
![](readme-resources/mr-details.png)
3. Merge the merge request with master

### Merge vs rebase
1. Create a branch `git checkout -b MergeExample`
2. Commit some changes
3. Go back to the master/main
4. Commit some changes so that the history between branches splits
5. Checkout master
6. Merge feature branch on master by `git merge MergeExample`
7. Create a branch `git checkout -b RebaseExample`
8. Go back to the master/main
9. Commit some changes so that the history between branches splits
10. Checkout master
11. Rebase master on top of feature branch by `git rebase RebaseExample`
12. Observe the changes in the log graph `git log --oneline --graph`

### Rebasing local master onto remote master
1. Checkout master and prepare new set of changes
```shell
git checkout main
#make a change on the repository
git add -A
git commit -m "Will result in a fail"
git push
```
Notice that the push fails with git complaining about your local branch being behind the upstream
2. Rebase local master onto remote master and try push again
```shell
git rebase
```
Notice that the push now succeeds

### Log
1. Check latest changes by running
```shell
git log -10 #print last 10 changes 
```
2. Check the content of the last change
```shell
git log -1 --stat
```
3. Display history graph
```shell
git log --oneline --graph
```
![](readme-resources/log-graph.png)
4. Other applications
```shell
git log --author=${AUTHOR} --since=${YEAR}-${MONTH}-01 --until=${NEXT_MONTH_YEAR}-${NEXT_MONTH}-01 --pretty=oneline --abbrev-commit
```
5. Read the actual change
```shell
git log -p
```

### Interactive rebase
1. Prepare a couple of commits
2. Find your commits in the log
```shell
git log -5
```
3. Run interactive rebase and have a look at available actions
```shell
git rebase -i HEAD~4 #perform interactive rebase 4 commits deep
# Commands:
# p, pick <commit> = use commit
# r, reword <commit> = use commit, but edit the commit message
# e, edit <commit> = use commit, but stop for amending
# s, squash <commit> = use commit, but meld into previous commit
# f, fixup <commit> = like "squash", but discard this commit's log message
# x, exec <command> = run command (the rest of the line) using shell
# b, break = stop here (continue rebase later with 'git rebase --continue')
# d, drop <commit> = remove commit
# l, label <label> = label current HEAD with a name
# t, reset <label> = reset HEAD to a label
# m, merge [-C <commit> | -c <commit>] <label> [# <oneline>]
# .       create a merge commit using the original merge commit's
# .       message (or the oneline, if no original merge commit was
# .       specified). Use -c <commit> to reword the commit message.
```
4. Try squashing, fixup, rework
5. Run `git log` to observe how the history ended up looking
6. Run interactive rebase and try splitting a commit into 2
```shell
git rebase -i HEAD~4
#choose edit or e option on one of the commits
#
```

### Reset
1. Push your local commit to remote if not necessary
2. Prepare a couple of commits
3. Reset repository to a state from before your latest changes retaining code in commits and adding it to index
```shell
git reset --soft HEAD~2
```
4. Make a single commit from changes gathered in the index
5. Reset your local master to the remote state
```shell
git reset --hard origin/master
```
6. Repeat these exercises introducing changes on a branch. When restoring pristine branch state try resetting against your local master instead of remote

### Revert
1. Run `git log --oneline --stat` to find a change that you would like to revert, eg.
```shell
4d04d31 (Lvl2Branch) Lvl2Branch commit
 src/main/resources/application.properties | 2 +-
```
2. Run `git revert 4d04d31` and observe the changes in the repository content and history

### Reflog
1. Restore a commit made on an "accidentally" deleted branch
2. Create a branch and make a commit
3. "Accidentally" delete a branch with `git branch -D AccidentallyDeletedBranchWithHeavyAndVeryImportantChanges` which forces a branch deletion
4. Run `git reflog`
```shell
C02ZK6ADLVDL:git-training krzysztof.fus$ git reflog
acdc5af (HEAD -> main, origin/main, origin/HEAD) HEAD@{0}: checkout: moving from AccidentallyDeletedBranchWithHeavyAndVeryImportantChanges to main
f47d89d HEAD@{1}: commit: A crucial change
acdc5af (HEAD -> main, origin/main, origin/HEAD) HEAD@{2}: checkout: moving from main to AccidentallyDeletedBranchWithHeavyAndVeryImportantChanges
acdc5af (HEAD -> main, origin/main, origin/HEAD) HEAD@{3}: commit: Revert
```
5. Checkout the deleted change
```shell
git checkout f47d89d
```
6. Retain commit by switching to a new branch 
```shell
git switch -c NoMoreMistakes
```
7. Push your change and create a merge request in gitlab

### Intellij tools
1. Creating branches
2. Versioning new files
3. Using shelf
4. Committing
5. Pushing changes
6. Reverting commits
7. Browsing history

## Useful links
1. [Explain shell](https://explainshell.com/) - Website explaing shell commands and their arguments (basically an online `man`)
2. [Gitlab flavoured markdown](https://docs.gitlab.com/ee/user/markdown.html) - markdown manual extended with gitlab specific features
