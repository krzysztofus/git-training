package org.krzysztofus.gittraining;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitTrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitTrainingApplication.class, args);
		System.out.println("Hello world!");
		//A crucial comment! Impossible to recreate from scratch!
		//More content
		//Another one
	}

}
